import uuid
from datetime import datetime, timedelta

import requests
from django.core.mail import EmailMessage, EmailMultiAlternatives
from icecream import ic

token = uuid.uuid4()
created_at = datetime.now()
expired_at = created_at + timedelta(minutes=20)

# when this is call
updated_at = datetime.now()

email = EmailMessage(
    "Hello",
    "Body goes here",
    "from@example.com",
    ["to1@example.com", "to2@example.com"],
    ["bcc@example.com"],
    reply_to=["another@example.com"],
    headers={"Message-ID": "foo"},
)

email = EmailMessage(
    "Subject here",  # subject
    "Here is the message.",  # message
    "from@example.com",  # from_email
    ["to@example.com"],  # recipient_list
    ["bcc@example.com"],  # bcc
    reply_to=["another@example.com"],  # reply_to
    headers={"Message-ID": "foo"},  # headers
    cc=["cc@example.com"],  # cc
)
# email.send()

# subject, from_email, to = (
#     "Test email",
#     "ledat.fake@gmail.com",
#     "hsddung92.lpdat@gmail.com",
# )
# text_content = "This is an important message."
# html_content = "<p>This is an <strong>important</strong> message.</p>"
# msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
# msg.attach_alternative(html_content, "text/html")
# msg.send()


# Using Set comprehensions
# for constructing output set

state = ["Gujarat", "Maharashtra", "Rajasthan"]
capital = ["Gandhinagar", "Mumbai", "Jaipur"]

dict_using_comp = {key: value for (key, value) in zip(state, capital)}

ic(dict_using_comp)
print("")


def send_simple_message():
    return requests.post(
        "https://api.mailgun.net/v3/sandbox20df732f370145dcafabea32a8a049b6.mailgun.org/messages",
        auth=("api", "af6582183dc5d35cec1b2b557e74e4e9-5d9bd83c-215aa9d7"),
        data={
            "from": "Mailgun <postmaster@sandbox20df732f370145dcafabea32a8a049b6.mailgun.org>",
            "to": ["Le Phuoc Dat <ledat.fake@gmail.com>"],
            "subject": "Verification",
            "text": "<h1>This is a title</h1>Congratulations Le Phuoc Dat, you just sent an email with Mailgun!  You are truly awesome!",
        },
    )


# You can see a record of this email in your logs: https://app.mailgun.com/app/logs.

# You can send up to 300 emails/day from this sandbox server.
# Next, you should add your own domain so you can send 10000 emails/month for free.


send_simple_message()
