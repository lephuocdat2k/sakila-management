from rest_framework import routers

from .views import RegisterViewSet

router = routers.DefaultRouter()
router.register(r"register", RegisterViewSet, basename="register")
urlpatterns = router.urls
