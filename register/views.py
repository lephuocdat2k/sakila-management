from django.contrib.auth.models import User
from rest_framework import status, viewsets
from rest_framework.response import Response

from .serializers import UserSerializer


class RegisterViewSet(viewsets.ViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            response_data = {
                "user": {
                    "username": user.username,
                    "email": user.email,
                    "password": user.password,
                    "is_staff": user.is_staff,
                    "is_superuser": user.is_superuser,
                },
                "message": "User created successfully",
            }
            return Response(response_data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
