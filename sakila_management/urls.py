from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from login.views import LoginView
from logout.views import LogoutView
from register.views import RegisterViewSet
from rental import views
from upload.views import FileUploadView

from .swagger import schema_view

router = routers.DefaultRouter()
router.register(r"register", RegisterViewSet, basename="register")
router.register(r"rental", views.RentalViewSet, basename="rental")
router.register(r"customer", views.CustomerViewSet, basename="customer")
router.register(r"inventory", views.InventoryViewSet, basename="inventory")
router.register(r"staff", views.StaffViewSet, basename="staff")

urlpatterns = [
    path("admin/", admin.site.urls),
    path(
        "api-auth/", include("rest_framework.urls", namespace="rest_framework")
    ),
    path(
        "swagger/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path(
        "redoc/",
        schema_view.with_ui("redoc", cache_timeout=0),
        name="schema-redoc",
    ),
    path("api/token/", LoginView.as_view(), name="token_obtain_pair"),
    path(
        "api/token/refresh/", TokenRefreshView.as_view(), name="token_refresh"
    ),
    path("api/login/", LoginView.as_view(), name="token_obtain_pair"),
    path("api/logout/", LogoutView.as_view(), name="logout"),
    path("api/", include(router.urls)),
    path("api/upload/", FileUploadView.as_view(), name="file-upload"),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
