import os
from pathlib import Path

import environ
from dotenv import load_dotenv
from icecream import ic

BASE_DIR = Path(__file__).resolve().parent.parent

env = environ.Env()
environ.Env.read_env(os.path.join(BASE_DIR, ".env"))
ic(env("EMAIL_HOST"))


# load_dotenv()
# ic(os.environ.get('EMAIL_HOST'))
