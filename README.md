# Installation

1. Install python 3.x : https://www.python.org/downloads/

- p/s: Remember: Add python to path

2. Create virual environment

- `python -m venv env`

- Activate environment
    - Window: `env\Scripts\activate`
    - Mac/Linux: `source env/bin/activate`

3. Go to main project

- `cd sakila_management/`

4. Install requirements package:

- `pip install -r requirements.txt`

5. Connect MySql

- *I've integrated the railway's MySQL Database show you don't need change anthing to connect to the online database*

- Unless you want to connect locally yourself then you need to change USERNAME, PASSWORD in ***settings.py*** of project

6. Run server:

- `python manage.py runserver`

# Deployment

- AWS EC2 Public IP: http://75.101.246.24:8000/
- Swagger API's Documentation: http://75.101.246.24:8000/swagger/
- Redoc API's Documentation: http://75.101.246.24:8000/redoc/

- Rental list items: http://75.101.246.24:8000/api/rental/
- Rental 1 item: http://75.101.246.24:8000/api/register/
- Rental register: http://75.101.246.24:8000/api/register/
- Rental login: http://75.101.246.24:8000/api/login/
- Rental logout: http://75.101.246.24:8000/api/logout/
