from rest_framework import serializers

from .models import Customer, Inventory, Rental, Staff


class InventorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Inventory
        fields = "__all__"


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = "__all__"


class StaffSerializer(serializers.ModelSerializer):
    class Meta:
        model = Staff
        fields = "__all__"


class RentalSerializer(serializers.ModelSerializer):
    # inventory = InventorySerializer()
    # customer = CustomerSerializer()
    # staff = StaffSerializer()

    class Meta:
        model = Rental
        fields = "__all__"
