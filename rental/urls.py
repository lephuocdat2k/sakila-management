from rest_framework.routers import DefaultRouter

from rental import views

router = DefaultRouter()
router.register(r"rental", views.RentalViewSet, basename="rental")
router.register(r"customer", views.CustomerViewSet, basename="customer")
router.register(r"inventory", views.InventoryViewSet, basename="inventory")
router.register(r"staff", views.StaffViewSet, basename="staff")
urlpatterns = router.urls
