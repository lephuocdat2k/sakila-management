from datetime import timedelta

import django_filters
from django_filters import rest_framework as filters
from django_filters.filters import DateFromToRangeFilter

from .models import Rental


class RentalFilter(django_filters.FilterSet):
    # rental_date = filters.DateFilter(field_name='rental_date', method=get_date)
    # rental_date = filters.CharFilter(field_name='category__name', lookup_expr='icontains')

    start_date = DateFromToRangeFilter(
        field_name="rental_date",
        widget=django_filters.widgets.RangeWidget(attrs={"type": "date"}),
        method="filter_by_day",
    )

    def filter_by_day(self, queryset, name, value):
        # Get start and end date from the range widget
        start_date = value.start
        end_date = value.stop

        # Filter the queryset for dates between start and end date
        if start_date and end_date:
            end_date += timedelta(days=1)
            queryset = queryset.filter(
                **{
                    name + "__range": (start_date, end_date),
                }
            )
        return queryset

    class Meta:
        model = Rental
        fields = ["staff"]
