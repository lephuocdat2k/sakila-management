import uuid

from rest_framework import (
    parsers,
    permissions,
    serializers,
    status,
    views,
    viewsets,
)
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import FileUploadModel


class FileUploadSerializer(serializers.ModelSerializer):
    class Meta:
        model = FileUploadModel
        fields = "__all__"


class FileUploadView(views.APIView):
    parser_classes = [parsers.MultiPartParser, parsers.FormParser]

    def post(self, request, format=None):
        # print(request.data)
        serializers = FileUploadSerializer(data=request.data)
        if serializers.is_valid():
            serializers.save()
            print(serializers)
            return Response(serializers.data, status=200)
        return Response(serializers.errors, status=400)

    def create(self, request):
        print(request.data)
