import uuid
from datetime import datetime

from django.db import models

# Create your models here.


def upload_to(instance, filename):
    datetime_now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    return f"images/{datetime_now}-{filename}"


class FileUploadModel(models.Model):
    file = models.FileField(upload_to=upload_to)
    title = models.CharField(max_length=100, null=True)
