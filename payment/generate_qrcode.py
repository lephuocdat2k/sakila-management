import os
import webbrowser

import crcmod.predefined
import qrcode


def generate_crc(data):
    crc16_func = crcmod.predefined.mkCrcFun("crc-ccitt-false")

    # Calculate CRC for the data
    crc_value = crc16_func(data.encode())

    # Convert CRC to Hex
    crc_hex = hex(crc_value)[2:].upper()

    return crc_hex


def length_code(data):
    return len(data) if len(data) > 9 else f"0{len(data)}"


def create_payment_qr_code(bank, account_number, amount, description):
    # payment_info = r"""
    # 00 02 01
    # 01 02 12
    # 38 57 0010A000000727 01 27 0006 970403 0113"0011012345678" 0208QRIBFTTA
    # 53 03 704
    # 54 06 180000
    # 58 02 VN
    # 62 34 01 07NPS6869 08 19thanh toan don hang"
    # 63 04 2E2E"""

    # payment_info = r"""
    # 000201
    # 01 02 12
    # 38 54 0010A000000727 01 24(len(toan bo tru ma dich vu)) 00 06 970436(ma_ngan hang) 01 10(len) "1037102700" 0208QRIBFTTA
    # 53 03 704
    # 54 06 100000
    # 58 02 VN
    # 62 07 08 03(len) "abc"
    # 63 04 9A0A"""

    # payment_info = f"00020101021238540010A00000072701{length_code(f'0006{bank}01{length_code(account_number)}{account_number}')}0006{bank}01{length_code(account_number)}{account_number}0208QRIBFTTA530370454{length_code(amount)}{amount}5802VN620708{length_code(description)}{description}6304"
    payment_info = f"00020101021238540010A00000072701{length_code(f'0006{bank}01{length_code(account_number)}{account_number}')}0006{bank}01{length_code(account_number)}{account_number}0208QRIBFTTA530370454{length_code(amount)}{amount}5802VN62{length_code(f'0107NPS686908{length_code(description)}{description}')}0107NPS686908{length_code(description)}{description}6304"

    crc = generate_crc(payment_info)
    payment_info = f"{payment_info}{crc}"

    print(payment_info)

    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=4,
    )
    qr.add_data(payment_info)
    qr.make(fit=True)

    qr_code_image = qr.make_image(fill_color="black", back_color="white")

    return qr_code_image


# Banks bin codes: https://api.vietqr.io/v2/banks
payment_id = 10
bank_name = "970436"
account_number = "9869570027"
payment_description = f"sakila_payment_{payment_id}"
amount = ""

# qr_code = create_payment_qr_code(
#     bank_name, account_number, amount, payment_description
# )
# qr_code.save(f"{os.path.dirname(__file__)}/payment_qr_code.png")

if __name__ == "__main__":
    BANK_ID = "970436"
    ACCOUNT_NO = "9869570027"
    TEMPLATE = "compact2"
    AMOUNT = "1000000"
    DESCRIPTION = "sakila_payment".replace(" ", "%20")
    ACCOUNT_NAME = "le phuoc dat".replace(" ", "%20")

    img_url = f"https://img.vietqr.io/image/{BANK_ID}-{ACCOUNT_NO}-{TEMPLATE}.png?amount={AMOUNT}&addInfo={DESCRIPTION}&accountName={ACCOUNT_NAME}"
    webbrowser.open(img_url)
